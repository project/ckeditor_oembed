<?php

namespace Drupal\ckeditor_oembed\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use GuzzleHttp\Client;
use GuzzleHttp\Exception as GuzzleException;

/**
 * Provides a setting UI for an embed Tweet.
 *
 * @see https://developer.twitter.com/en/docs/tweets/post-and-engage/api-reference/get-statuses-oembed.html
 */
class TweetEmbedForm extends FormBase {

  protected $providerResponse = NULL;
  protected $providerName = 'Twitter';
  protected $providerOEmberUrl = 'https://publish.twitter.com/oembed';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tweet_embed_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Editor $editor = NULL) {
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#prefix'] = '<div id="editor-oembed-tweet-dialog">';
    $form['#suffix'] = '</div>';
    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Tweet URL'),
      '#description' => $this->t('Copy and paste here the URL of twitter status.'),
      '#required' => TRUE,
      '#weight' => '0',
    ];
    $form['maxwidth'] = [
      '#type' => 'number',
      '#title' => $this->t('Max Width'),
      '#description' => $this->t('The maximum width of a rendered Tweet in whole pixels. . Implementations that need consistent heights for Tweets should refer to the hide_thread and hide_media parameters below.'),
      '#default_value' => 325,
      '#attributes' => [
        'min' => 220,
        'max' => 550,
      ],
      '#access' => TRUE,
      '#weight' => '1',
    ];
    $form['hide_media'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Media'),
      '#description' => $this->t('When set to true links in a Tweet are not expanded to photo, video, or link previews.'),
      '#default_value' => FALSE,
      '#weight' => '2',
    ];
    $form['hide_thread'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide thread'),
      '#description' => $this->t('When set to true a collapsed version of the previous Tweet in a conversation thread will not be displayed when the requested Tweet is in reply to another Tweet.'),
      '#default_value' => FALSE,
      '#access' => TRUE,
      '#weight' => '3',
    ];
    $form['omit_script'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Omit script'),
      '#description' => $this->t('When set to true  the &lt;script&gt; responsible for loading widgets.js will not be returned.'),
      '#default_value' => FALSE,
      '#access' => TRUE,
      '#weight' => '4',
    ];
    $form['align'] = [
      '#type' => 'select',
      '#title' => $this->t('Align'),
      '#description' => $this->t('Specifies whether the embedded Tweet should be floated left, right, or center in the page relative to the parent element.'),
      '#options' => [
        'left' => $this->t('left'),
        'right' => $this->t('right'),
        'center' => $this->t('center'),
        'none' => $this->t('none'),
      ],
      '#size' => 1,
      '#default_value' => 'none',
      '#access' => TRUE,
      '#weight' => '5',
    ];
    $form['related'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Related'),
      '#description' => $this->t('A comma-separated list of Twitter usernames related to your content. This value will be forwarded to Tweet action intents if a viewer chooses to reply, like, or retweet the embedded Tweet.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#access' => TRUE,
      '#weight' => '6',
    ];
    $form['lang'] = [
      '#type' => 'select',
      '#title' => $this->t('Language'),
      '#description' => $this->t('Request returned HTML and a rendered Tweet in the specified Twitter language supported by embedded Tweets.'),
      '#options' => [
        'en' => $this->t('English'),
        'ar' => $this->t('Arabic'),
        'bn' => $this->t('Bengali'),
        'cs' => $this->t('Czech'),
        'da' => $this->t('Danish'),
        'de' => $this->t('German'),
        'el' => $this->t('Greek'),
        'es' => $this->t('Spanish'),
        'fa' => $this->t('Persian'),
        'fi' => $this->t('Finnish'),
        'fil' => $this->t('Filipino'),
        'fr' => $this->t('French'),
        'he' => $this->t('Hebrew'),
        'hi' => $this->t('Hindi'),
        'hu' => $this->t('Hungarian'),
        'id' => $this->t('Indonesian'),
        'it' => $this->t('Italian'),
        'ja' => $this->t('Japanese'),
        'ko' => $this->t('Korean'),
        'msa' => $this->t('Malay'),
        'nl' => $this->t('Dutch'),
        'no' => $this->t('Norwegian'),
        'pl' => $this->t('Polish'),
        'pt' => $this->t('Portuguese'),
        'ro' => $this->t('Romanian'),
        'ru' => $this->t('Russian'),
        'sv' => $this->t('Swedish'),
        'th' => $this->t('Thai'),
        'tr' => $this->t('Turkish'),
        'uk' => $this->t('Ukrainian'),
        'ur' => $this->t('Urdu'),
        'vi' => $this->t('Vietnamese'),
        'zh-cn' => $this->t('Chinese (Simplified)'),
        'zh-tw' => $this->t('Chinese (Traditional)'),
      ],
      '#size' => 1,
      '#default_value' => 'en',
      '#access' => TRUE,
      '#weight' => '7',
    ];
    $form['theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Theme'),
      '#description' => $this->t('When set to dark, the Tweet is displayed with light text over a dark background.'),
      '#options' => ['light' => $this->t('light'), 'dark' => $this->t('dark')],
      '#size' => 1,
      '#default_value' => 'light',
      '#access' => TRUE,
      '#weight' => '8',
    ];
    $form['link_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Link color'),
      '#description' => $this->t('Adjust the color of Tweet text links with a hexadecimal color value.'),
      '#default_value' => '#dc291b',
      '#access' => TRUE,
      '#weight' => '9',
    ];
    $form['widget_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Widget type'),
      '#description' => $this->t('Set to video to return a Twitter Video embed for the given Tweet.'),
      '#options' => ['' => $this->t('Content'), 'video' => $this->t('video')],
      '#default_value' => '',
      '#size' => 1,
      '#access' => TRUE,
      '#weight' => '10',
    ];
    $form['dnt'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('DNT'),
      '#description' => $this->t('When set to true, the Tweet and its embedded page on your site are not used for purposes that include personalized suggestions and personalized ads.'),
      '#default_value' => FALSE,
      '#access' => TRUE,
      '#weight' => '11',
    ];
    $form['tweet'] = [
      '#type' => 'item',
      '#default_value' => '',
      '#value' => '',
    ];
    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => '555',
    ];
    $form['actions']['save_modal'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      // No regular submit-handler. This form only works via JavaScript.
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitForm',
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // Assert the twitter status url is valid.
    if (!$form_state->getValue('url') || empty($form_state->getValue('url')) || (FALSE === strpos($form_state->getValue('url'), 'https://twitter.com'))) {
      $form_state->setErrorByName('url', $this->t('This URL should be a link to a Twitter status. <br/> The URL should start by https://twitter.com .'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#editor-oembed-tweet-dialog', $form));
    }
    else {
      $this->getproviderResponse($form, $form_state);
      $form_state->setValue('tweet', $this->providerResponse);
      $response->addCommand(new EditorDialogSave($form_state->getValues()));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;

  }

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return string
   */
  public function getproviderResponse(&$form, FormStateInterface $form_state) {
    if (NULL !== $this->providerResponse) {
      return $this->providerResponse;
    }
    $client = new Client();
    try {
      $response = $client->get($this->providerOEmberUrl,
          [
            'query' => [
              'url' => $form_state->getValue('url'),
              'maxwidth' => $form_state->getValue('maxwidth') ?? $form['maxwidth']['#default_value'],
              'hide_media' => $form_state->getValue('hide_media') ?? $form['hide_media']['#default_value'],
              'hide_thread' => $form_state->getValue('hide_thread') ?? $form['hide_thread']['#default_value'],
              'omit_script' => $form_state->getValue('omit_script') ?? $form['omit_script']['#default_value'],
              'align' => $form_state->getValue('align') ?? $form['align']['#default_value'],
              'related' => $form_state->getValue('related') ?? $form['related']['#default_value'],
              'lang' => $form_state->getValue('lang') ?? $form['lang']['#default_value'],
              'theme' => $form_state->getValue('theme') ?? $form['theme']['#default_value'],
              'link_color' => $form_state->getValue('link_color') ?? $form['link_color']['#default_value'],
              'widget_type' => $form_state->getValue('widget_type') ?? $form['widget_type']['#default_value'],
              'dnt' => $form_state->getValue('dnt') ?? $form['dnt']['#default_value'],
            ],
          ]);
      $data = json_decode($response->getBody()->getContents());
    }
    catch (GuzzleException $e) {
      watchdog_exception('ckeditor_oembed', $e->getMessage());
    }
    $this->providerResponse = $data->html;
    return $this->providerResponse;
  }

}
