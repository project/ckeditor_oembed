<?php

namespace Drupal\ckeditor_oembed\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use GuzzleHttp\Client;
use GuzzleHttp\Exception as GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a setting UI for an embed Instagram post.
 *
 * @see https://www.instagram.com/developer/embedding/#oembed
 */
class InstagramEmbedForm extends FormBase {

  protected $providerResponse = NULL;
  protected $providerName = 'Instagram';
  protected $providerOEmberUrl = 'https://graph.facebook.com/instagram_oembed';

  /**
   * The ConfigFactory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a new ConfigFactory object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The ConfigFactory.
   */
  public function __construct(ConfigFactory $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'Instagram_embed_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Editor $editor = NULL) {
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#prefix'] = '<div id="editor-oembed-instagram-dialog">';
    $form['#suffix'] = '</div>';
    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Instagram status URL'),
      '#description' => $this->t('A short link, like http://instagr.am/p/fA9uwTtkSN/.'),
      '#required' => TRUE,
      '#weight' => '0',
    ];
    $form['maxwidth'] = [
      '#type' => 'number',
      '#title' => $this->t('Max Width'),
      '#description' => $this->t('Maximum width of returned media. This value is optional and must be greater than 320. Note that the maxheight parameter is not supported and always returns null. This is because the embed code is responsive and its height varies depending on its width and lenght of the caption.'),
      '#default_value' => 700,
      '#attributes' => ['min' => 320],
      '#access' => FALSE,
      '#weight' => '1',
    ];
    $form['hidecaption'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide caption'),
      '#description' => $this->t('If set to true, the embed code hides the caption. Defaults to false.'),
      '#default_value' => FALSE,
      '#weight' => '0',
    ];
    $form['omitscript'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Omit script'),
      '#description' => $this->t('If set to true, the embed code does not include the script tag. This is useful for websites that want to handle the loading of the embeds.js script by themselves. To manually initialize the embed code, call the JS function instgrm.Embeds.process().'),
      '#default_value' => FALSE,
      '#access' => FALSE,
      '#weight' => '0',
    ];
    $form['instagram'] = [
      '#type' => 'item',
      '#default_value' => '',
      '#value' => '',
    ];
    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => '555',
    ];
    $form['actions']['save_modal'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      // No regular submit-handler. This form only works via JavaScript.
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitForm',
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // Assert the twitter status url is valid.
    if (!$form_state->getValue('url') || empty($form_state->getValue('url')) || (FALSE === strpos($form_state->getValue('url'), 'https://www.instagram.com/'))) {
      $form_state->setErrorByName('url', $this->t('This URL should be a link to an Instagram status. <br/> The URL should start by https://www.instagram.com/ .'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#editor-oembed-instagram-dialog', $form));
    }
    else {
      $this->getproviderResponse($form, $form_state);
      $form_state->setValue('instagram', $this->providerResponse);
      $response->addCommand(new EditorDialogSave($form_state->getValues()));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;

  }

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return string
   *   HTML to be embed in the editor.
   */
  public function getproviderResponse(&$form, FormStateInterface $form_state) {
    if (NULL !== $this->providerResponse) {
      return $this->providerResponse;
    }
    $client = new Client();
    try {
      $config = $this->configFactory->get('ckeditor_oembed.settings');
      $response = $client->get($this->providerOEmberUrl,
        [
          'query' => [
            'url' => $form_state->getValue('url'),
            'access_token' => $config->get('app_id') . '|' . $config->get('secret_key'),
            'maxwidth' => $form_state->getValue('maxwidth') ?? $form['maxwidth']['#default_value'],
            'hidecaption' => $form_state->getValue('hidecaption') ?? $form['hidecaption']['#default_value'],
            'omitscript' => $form_state->getValue('omitscript') ?? $form['omitscript']['#default_value'],
          ],
        ]);
      $data = json_decode($response->getBody()->getContents());
    }
    catch (GuzzleException $e) {
      watchdog_exception('ckeditor_oembed', $e->getMessage());
    }
    $this->providerResponse = $data->html;
    return $this->providerResponse;
  }

}
