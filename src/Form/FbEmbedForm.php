<?php

namespace Drupal\ckeditor_oembed\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use GuzzleHttp\Client;
use GuzzleHttp\Exception as GuzzleException;

/**
 * Provides a setting UI for an embed Facebook post.
 *
 * @see https://developers.facebook.com/docs/plugins/oembed-endpoints?locale=fr_FR
 */
class FbEmbedForm extends FormBase {

  protected $providerResponse = NULL;
  protected $providerName = 'Facebook';
  protected $providerPostOEmberUrl = 'https://www.facebook.com/plugins/post/oembed.json/';
  protected $providerVideoOEmberUrl = 'https://www.facebook.com/plugins/video/oembed.json/';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'Fb_embed_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Editor $editor = NULL) {
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#prefix'] = '<div id="editor-oembed-fb-dialog">';
    $form['#suffix'] = '</div>';
    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Media types'),
      '#description' => $this->t('Select media'),
      '#options' => ['post' => $this->t('full posts'), 'video' => $this->t('Video')],
      '#size' => 1,
      '#default_value' => 'post',
      '#weight' => '0',
    ];
    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Facebook Public Publication URL'),
      '#description' => $this->t('An FB publication link, like https://www.facebook.com/SACD.FR/posts/2094537710583076.'),
      '#required' => TRUE,
      '#weight' => '1',
    ];
    $form['maxwidth'] = [
      '#type' => 'number',
      '#title' => $this->t('Max Width'),
      '#description' => $this->t('Maximum width of returned media. This value is optional and must be greater than 500.'),
      '#default_value' => 500,
      '#attributes' => ['min' => 320],
      '#access' => FALSE,
      '#weight' => '2',
    ];
    $form['omitscript'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Omit script'),
      '#description' => $this->t('If set to true, the embed code does not include the script tag. This is useful for websites that want to handle the loading of the embeds.js script by themselves.'),
      '#default_value' => TRUE,
      '#access' => FALSE,
      '#weight' => '3',
    ];
    $form['fb'] = [
      '#type' => 'item',
      '#default_value' => '',
      '#value' => '',
    ];
    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => '555',
    ];
    $form['actions']['save_modal'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      // No regular submit-handler. This form only works via JavaScript.
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitForm',
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // Assert the twitter status url is valid.
    if (!$form_state->getValue('url') || empty($form_state->getValue('url')) || (FALSE === strpos($form_state->getValue('url'), 'https://www.facebook.com/'))) {
      $form_state->setErrorByName('url', $this->t('This URL should be a link to a FB video or status. <br/> The URL should start by https://www.facebook.com/ .'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#editor-oembed-fb-dialog', $form));
    }
    else {
      $this->getProviderResponse($form, $form_state);
      $form_state->setValue('fb', $this->providerResponse);
      $response->addCommand(new EditorDialogSave($form_state->getValues()));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

  /**
   * Get Provider Response.
   *
   * @param $form
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return string
   *   $providerResponse.
   */
  public function getProviderResponse(&$form, FormStateInterface $form_state) {
    if (NULL !== $this->providerResponse) {
      return $this->providerResponse;
    }
    $client = new Client();
    try {
      $response = $client->get(($form_state->getValue('type') == 'post') ? $this->providerPostOEmberUrl : $this->providerVideoOEmberUrl,
          [
            'query' => [
              'url' => $form_state->getValue('url'),
              'maxwidth' => $form_state->getValue('maxwidth') ?? $form['maxwidth']['#default_value'],
              'omitscript' => $form_state->getValue('omitscript') ?? $form['omitscript']['#default_value'],
            ],
          ]);
      $data = json_decode($response->getBody()->getContents());
    }
    catch (GuzzleException $e) {
      watchdog_exception('ckeditor_oembed', $e->getMessage());
    }
    $this->providerResponse = $data->html;
    return $this->providerResponse;
  }

}
