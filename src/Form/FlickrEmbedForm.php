<?php

namespace Drupal\ckeditor_oembed\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\editor\Entity\Editor;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FlickrEmbedForm.
 *
 * @see https://oembed.com
 */
class FlickrEmbedForm extends FormBase {

  /**
   * Response from flickr oembed service.
   *
   * @var null
   */
  protected $providerResponse = NULL;

  /**
   * Oembed url.
   *
   * @var null
   */
  protected $providerOEmberUrl = 'http://www.flickr.com/services/oembed/';

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a FlickrEmbedForm object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'flickr_embed_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Editor $editor = NULL) {
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#prefix'] = '<div id="editor-oembed-flickr-dialog">';
    $form['#suffix'] = '</div>';

    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Flickr status URL'),
      '#description' => $this->t('The link from browser.'),
      '#required' => TRUE,
      '#weight' => '0',
    ];
    $form['maxwidth'] = [
      '#type' => 'number',
      '#title' => $this->t('Max Width'),
      '#description' => $this->t('The maximum width of an image in pixels.'),
      '#default_value' => 1024,
      '#access' => FALSE,
    ];
    $form['actions'] = [
      '#type' => 'actions',
      'save_modal' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#ajax' => [
          'callback' => '::submitForm',
          'event' => 'click',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // Assert the flickr status url is valid.
    if (!$form_state->getValue('url') || empty($form_state->getValue('url')) || (FALSE === strpos($form_state->getValue('url'), 'https://www.flickr.com/'))) {
      $form_state->setErrorByName('url', $this->t('This URL should be a link to an Flickr page. <br/> The URL should start by https://www.flickr.com/ .'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#editor-oembed-flickr-dialog', $form));
    }
    else {
      $this->getProviderResponse($form, $form_state);
      $form_state->setValue('flickr', $this->providerResponse);
      $response->addCommand(new EditorDialogSave($form_state->getValues()));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

  /**
   * Get response from flickr service.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return string
   *   Response from flickr.
   */
  public function getProviderResponse(array &$form, FormStateInterface $form_state) {
    if ($this->providerResponse !== NULL) {
      return $this->providerResponse;
    }
    try {
      $response = $this->httpClient->get($this->providerOEmberUrl, [
        'query' => [
          'format' => 'json',
          'url' => $form_state->getValue('url'),
          'maxwidth' => $form_state->getValue('maxwidth') ?? $form['maxwidth']['#default_value'],
        ],
      ]);
      $data = Json::decode($response->getBody()->getContents());
    }
    catch (RequestException $e) {
      $this->messenger()->addError($e->getMessage());
    }
    $this->providerResponse = $data['html'] ?? '';
    return $this->providerResponse;
  }

}
