/**
 * @file
 * Plugin to embed Flickr images into the editor.
 */

(function ($, Drupal, drupalSettings, CKEDITOR) {
  'use strict';

  var insertContent;
  var saveCallback = function (data) {
    if (data.flickr) {
      var content = data.flickr;
      insertContent(content);
    }
  };
  CKEDITOR.plugins.add('flickr_ckeditor_button', {
    hidpi: true,

    beforeInit: function (editor) {
      editor.addCommand('embed_flickr', {
        modes: {wysiwyg: 1},
        canUndo: true,
        exec: function exec(editor) {
          let existingValues = {};
          let dialogSettings = {
            title: Drupal.t('Embed Flickr'),
            dialogClass: 'editor-oembed-flickr-dialog'
          };
          Drupal.ckeditor.openDialog(editor, Drupal.url('admin/ckeditor_oembed/flickr_embed/' + editor.config.drupal.format), existingValues, saveCallback, dialogSettings);
        }
      });
      editor.ui.addButton('flickr_ckeditor_button', {
        label: Drupal.t('Embed flickr'),
        command: 'embed_flickr',
        icon: this.path + 'images/icon.png'
      });
      insertContent = function (html) {
        editor.insertHtml(html);
      }
    },
    // The plugin initialization logic goes inside this method.
    init: function (editor) {
      // Register the flickr_ckeditor_button widget.
      editor.widgets.add('flickr_ckeditor_button', {
        // Check the elements that need to be converted to widgets.
        upcast: function (element) {
          return element.name == 'blockquote' && element.hasClass('flickr-media');
        }
      });
    }
  });
})(jQuery, Drupal, drupalSettings, CKEDITOR);
