/**
 * Inserting instagram post to the editor
 */
(function ($, Drupal, drupalSettings, CKEDITOR) {
  'use strict';

  CKEDITOR.plugins.add('instagram_ckeditor_button', {
    hidpi: true,
    beforeInit: function (editor) {
      editor.addCommand('embed_instagram', {
        modes: {wysiwyg: 1},
        canUndo: true,
        exec: function exec(editor) {
          let existingValues = {};
          let dialogSettings = {
            title: Drupal.t('Embed an Instagram'),
            dialogClass: 'editor-oembed-instagram-dialog'
          };
          var insertContent = function(html) {
            editor.insertHtml(html);
          };
          var saveCallback = function(data) {
            if(data.instagram) {
              var content = data.instagram;
              insertContent(content);
            }
          };
          Drupal.ckeditor.openDialog(editor, Drupal.url('admin/ckeditor_oembed/instagram_embed/' + editor.config.drupal.format), existingValues, saveCallback, dialogSettings);
        }
      });
      editor.ui.addButton('Instagram_ckeditor_button', {
        label: Drupal.t('Embed Instagram'),
        command: 'embed_instagram',
        icon: this.path + 'images/icon.png'
      });
    }
  });
})(jQuery, Drupal, drupalSettings, CKEDITOR);
