/**
 * Inserting FB post or video to the editor
 */
(function ($, Drupal, drupalSettings, CKEDITOR) {
  'use strict';

  CKEDITOR.plugins.add('fb_ckeditor_button', {
    hidpi: true,
    beforeInit: function (editor) {
      editor.addCommand('embed_fb', {
        modes: {wysiwyg: 1},
        canUndo: true,
        exec: function exec(editor) {
          let existingValues = {};
          let dialogSettings = {
            title: Drupal.t('Embed a Facebook public publication or a video.'),
            dialogClass: 'editor-oembed-fb-dialog'
          };
          var insertContent = function(html) {
            editor.insertHtml(html);
          };
          var saveCallback = function(data) {
            console.log(data);
            if(data.fb) {
              var content = data.fb;
              insertContent(content);
            }
          };
          Drupal.ckeditor.openDialog(editor, Drupal.url('admin/ckeditor_oembed/fb_embed/' + editor.config.drupal.format), existingValues, saveCallback, dialogSettings);
        }
      });
      editor.ui.addButton('Fb_ckeditor_button', {
        label: Drupal.t('Embed Facebook'),
        command: 'embed_fb',
        icon: this.path + 'images/icon.png'
      });
    }
  });
})(jQuery, Drupal, drupalSettings, CKEDITOR);
