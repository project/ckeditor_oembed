/**
 * Inserting tweet to the editor
 */
(function ($, Drupal, drupalSettings, CKEDITOR) {
  'use strict';
  
  CKEDITOR.plugins.add('twitter_ckeditor_button', {
    hidpi: true,
    beforeInit: function (editor) {
      editor.addCommand('embed_tweet', {
        modes: {wysiwyg: 1},
        canUndo: true,
        exec: function exec(editor) {
          let existingValues = {};
          let dialogSettings = {
            title: Drupal.t('Embed a Tweet'),
            dialogClass: 'editor-oembed-tweet-dialog'
          };
          var insertContent = function(html) {
            editor.insertHtml(html);
          }
          var saveCallback = function(data) {
            if(data.tweet) {
              var content = data.tweet;
              insertContent(content);
            }
          };
          Drupal.ckeditor.openDialog(editor, Drupal.url('admin/ckeditor_oembed/tweet_embed/' + editor.config.drupal.format), existingValues, saveCallback, dialogSettings);
        }
      });
      editor.ui.addButton('Twitter_ckeditor_button', {
        label: Drupal.t('Embed Tweet'),
        command: 'embed_tweet',
        icon: this.path + 'images/icon.png'
      });
    }
  });
})(jQuery, Drupal, drupalSettings, CKEDITOR);
