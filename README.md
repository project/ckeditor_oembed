# INTRODUCTION
The CKEditor OEmbed module adds easy support for Facebook, Tweet and Instagram posts integration 
using CKEditor.

# REQUIREMENTS
During content creation the author may add embed resources (Tweet, Instagram,
 etc.) hosted by other services (called the "content providers") in content 
 via CKEditor.
By default the plugin uses the Oembed service which supports over 1715 
content providers such as Youtube, Vimeo, Twitter, Insagram, Imgur, GitHub,
 and Google Maps.
In order to display tweets you need to enable blocquote tag inside the text 
format.
You may also use a specific media provider so the embeds are limited to the 
embed types it supports. Which it's Twitter and Instagram at the moment but
 more providers will be soon added.
# INSTALLATION
```ssh
composer require drupal/ckeditor_oembed 
```
# CONFIGURATION
You need to go to "admin/config/content/formats" and enable embeding buttons
for the format that you want.
Then you should allow custom HTML attributes used by providers scripts:
```html
<a href hreflang name id class> <em> <strong> <cite class> <blockquote cite 
class id data-*> <code class> <ul type class> <ol start type class> <li id
 class> <dl class> <dt class> <dd class> <h2 id class> <h3 id class> <h4 id 
 class> <h5 id class> <h6 id class> <u> <s> <img src alt data-entity-type
  data-entity-uuid class> <p class> <pre> <table class id> <caption> <tbody 
  class> <thead class> <tfoot class> <th class> <td class> <tr> <div id class
   data-*>
```
